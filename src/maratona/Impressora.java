package maratona;

import java.util.List;

public class Impressora {
	public static void imprimir(List<Equipe> equipes) {
		equipes.forEach(equipe -> System.out.println(equipe));
	}
}
